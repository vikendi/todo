// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require jquery
//= require_tree .
function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

const errorResponce = (function handleServerResponce () {
  const errors = arguments[0];

  $('.errorsBlock').remove();
  $(":input").removeClass("error");
  Object.keys(errors).map((key) => {
    const selector = '#user_'+key;
    $(selector).addClass("error")
  });

  $('.content').append(
    ' <div class="errorsBlock"></div>');
  for(const key in errors) {
    $('.errorsBlock').append( '<div class="userError alert alert-danger alert-dismissible fade show">' +
      '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
      ' <strong>Error! </strong>' + capitalizeFirstLetter(key) + ` ${errors[key][0]} `  +
      ' </div>'
    )
  }

  $(document).ready(function () {
    $(".close").click(function () {
      $(this).closest('.alert').remove()
    });
  });

  setTimeout( () => {
    $('.errorsBlock').remove();
  }, 5000)

});