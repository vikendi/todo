class ApplicationRecord < ActiveRecord::Base
  include ErrorsHandler
  self.abstract_class = true
end
