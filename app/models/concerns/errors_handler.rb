module ErrorsHandler
  extend ActiveSupport::Concern
  module ClassMethods
    errors_arr = {}
    define_method(:add_errors) { |errors| errors_arr = errors }
    define_method(:get_errors) { errors_arr }
  end
end