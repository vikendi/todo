Rails.application.routes.draw do
  resources :sessions, only: [:create, :destroy]
  resources :users
  get '/signup', to: 'users#new'
  get '/login', to: 'sessions#create'
  get '/logout', to: 'sessions#destroy'
  resources :todo_lists do
    resources :todo_items do
      member do
        patch :complete
      end
    end
  end

  root "todo_lists#index"

end
